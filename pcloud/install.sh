if hash pcloud 2> /dev/null; then
    echo "pcloud already installed"
    exit
fi

if [[ $QUIET -ne 1 ]]; then
    echo "Opening link to download qutebrowser."
    echo "Place the executable in ~/.local/bin."
    read -n 1 -p "Press enter when finished..."
    qutebrowser "https://www.pcloud.com/how-to-install-pcloud-drive-linux.html?download=electron-64"
fi
