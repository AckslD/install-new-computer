source install_functions.sh

# Install golang
case $LINUX in
    $UBUNTU)
        $INSTALL_FUNC golang
        ;;
    $ARCH)
        $INSTALL_FUNC go
        ;;
esac

mkdir -p ~/Dev/go
mkdir -p ~/Dev/golib
