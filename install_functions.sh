APT_QUERY_CMD="dpkg -s"
APT_INSTALL_CMD="sudo apt install -y"
PACMAN_QUERY_CMD="pacman -Qi"
PACMAN_INSTALL_CMD="sudo pacman -S --noconfirm --needed"
YAY_INSTALL_CMD="yay -S --noconfirm"

# Only export the "types", EndeavourOS and Arch are the both of type Arch
export UBUNTU="Ubuntu"
ENDEAVOUROS="EndeavourOS"
export ARCH="Arch"

# Get distribution ID
LINUX="$(lsb_release -a 2> /dev/null | grep "Distributor ID" | cut -c17-)"

case $LINUX in
    $UBUNTU)
        LINUX=$UBUNTU
        ;;
    $ARCH|$ENDEAVOUROS)
        LINUX=$ARCH
        ;;
    *)
        echo "unsupported linux $LINUX"
        exit 1
esac
export $LINUX

function is_installed() {
    PKG=$1
    if eval "$QUERY_CMD $PKG" > /dev/null 2>&1; then
        echo true
    else
        echo false
    fi
}

# Function for only installing the package if it is not already installed
function install() {
    INSTALL_CMD=$1
    # echo "install_cmd: $INSTALL_CMD"
    # Remove  the first arg
    shift
    for PKG in "$@"
    do
        # echo "pkg: $PKG"
        # echo "answer: $(is_installed $PKG)"
        if $(is_installed $PKG); then
            echo "Package $PKG already installed..."
        else
            echo "Installing $PKG..."
            eval "$INSTALL_CMD $PKG"
        fi
    done
}

function apt_install() {
    install "$APT_INSTALL_CMD" $@
}

function pacman_install() {
    install "$PACMAN_INSTALL_CMD" $@
}

function yay_install() {
    install "$YAY_INSTALL_CMD" $@
}

# Function for installing from a install script in a subfolder
function folder_install() {
    for folder in "$@"; do
        echo "Installing $folder..."
        bash $folder/install.sh
    done
}

# Function to install using cargo
function cargo_install() {
    for PKG in "$@"; do
        cargo install $PKG
    done
}

# Copies all bash scripts in bin-folders to ~/.local/bin and makes them executable
function copy_all_bin() {
    echo "Copying executables to ~/.local/bin"
    for bin_folder in $(find . -type d \( -name "bin" ! -wholename "*/arch/*" \)); do
        for bin in $bin_folder/*; do
            binname=$(basename -- "$bin")
            echo -e "\t$bin -> ~/.local/bin/$binname"
            cp $bin ~/.local/bin
            chmod +x ~/.local/bin/$binname
        done
    done
}

# Copy .service files etc and setup timers on startup
function setup_systemd_units() {
    echo "Setting up systemd user files"
    # Copy service and timers
    SYSTEMD_USER_DIR=~/.config/systemd/user
    mkdir -p $SYSTEMD_USER_DIR
    find . -type f \( -name "*.service" -o -name "*.timer" \) -exec cp {} $SYSTEMD_USER_DIR \;
    find $SYSTEMD_USER_DIR -type f -name "*.service" -exec sed -i "s|\$HOME|$HOME|g" {} \;

    # Enable the timers
    find . -type f -name "*.timer" -exec systemctl enable --user $(basename {}) \;

    # Copy desktop files
    APPLICATIONS_DIR=~/.local/share/applications
    mkdir -p $APPLICATIONS_DIR
    find . -type f -name "*.desktop" -exec cp {} $APPLICATIONS_DIR \;
    find $APPLICATIONS_DIR -type f -name "*.desktop" -exec sed -i "s|\$HOME|$HOME|g" {} \;
}
