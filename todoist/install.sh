if hash todoist 2> /dev/null; then
    echo "todoist already installed"
    exit
fi

TMP_PATH=~/Dev/golib/src/github.com/sachaos
mkdir -p $TMP_PATH
cd $TMP_PATH
git clone https://github.com/sachaos/todoist.git
cd todoist
make install
cd
go get github.com/urfave/cli
