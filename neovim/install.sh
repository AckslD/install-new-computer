source install_functions.sh

# Install latest neovim
if [[ -f ~/.local/bin/nvim ]]; then
    echo "neovim already installed (~/.local/bin/nvim)"
else
    yay_install neovim-git
    # mkdir -p ~/downloads
    # wget https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage -P ~/downloads
    # chmod u+x ~/downloads/nvim.appimage
    # cp ~/downloads/nvim.appimage ~/.local/bin/nvim
    # chmod +x ~/.local/bin/nvim
fi

# Python support
case $LINUX in
    $UBUNTU)
        $INSTALL_FUNC python3-neovim
        ;;
    $ARCH)
        $INSTALL_FUNC fuse
        $INSTALL_FUNC python-neovim
        ;;
esac
