source install_functions.sh

# Make fish the shell
if ! hash fish 2> /dev/null; then
    sudo apt-add-repository ppa:fish-shell/release-3
    sudo apt-get update
    install fish
    sudo chsh -s $(which fish)
    echo "For fish to be the default shell you need to logout and in again..."
    read -n 1 -p "Press enter to continue..."
fi

# Install oh-my-fish and powerline fonts
if ! $(echo "type -q omf" | fish); then
    read -p "It seems omf is not installed correctly, do you want to remove ~/.local/share/omf and reinstall it..(y/n)" yn
    if [ "$yn" == "y" ]; then
        rm -rf ~/.local/share/omf
        curl -L https://get.oh-my.fish | fish
    fi
fi
apt_install fonts-powerline
