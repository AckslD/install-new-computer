[[ -d ~/.ssh ]] && exit
# Make an ssh key and copy to clipboard
if [[ $LINUX == $ARCH ]]; then
    $INSTALL_FUNC openssh
fi
mkdir -p ~/.ssh
if [ ! -f ~/.ssh/id_ed25519.pub ]; then
    ssh-keygen -t ed25519 -C "ssh@valleymnt.com"
fi
cat ~/.ssh/id_ed25519.pub | xclip -selection clipboard
echo "Please enter the ssh key which is in the clipboard to gitlab"
if [[ $FORCE -ne 1 ]]; then
    read -n 1 -p "Press enter when finished"
fi
