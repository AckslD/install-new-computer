which ncspot 2> /dev/null && exit
source install_functions.sh

read -p "Install ncspot (slow)? (y/n)" yn
if ! [[ "$yn" == "y" ]]; then
    exit
fi
case $LINUX in
    $UBUNTU)
        cargo_install ncspot
        ;;
    $ARCH)
        yay_install ncspot
        ;;
esac
