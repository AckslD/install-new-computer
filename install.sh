set -e

#!/usr/bin/env bash
source install_functions.sh

# Parse args
for arg in $@; do
    case $arg in
        -t|--terminal)
            export TERMINAL=1
            ;;
        -q|--quiet)
            export QUIET=1
            ;;
        -f|--force)
            export FORCE=1
            export QUIET=1
            ;;
        -p|--private)
            export PRIVATE=1
            ;;
    esac
done

if [[ $FORCE -eq 1 ]]; then
    yn="y"
else
    read -p "Found distro $LINUX, is this correct? (y/n)" yn
fi
if [ "$yn" == "n" ]; then
    echo "Aborting!"
    exit
fi

# Create apps folder
mkdir -p ~/apps
mkdir -p ~/.local/bin

case $LINUX in
    $UBUNTU)
        export INSTALL_FUNC="apt_install"
        export QUERY_CMD=$APT_QUERY_CMD
        # Update
        sudo apt update
        sudo apt upgrade -y
        ;;
    $ARCH)
        export INSTALL_FUNC="pacman_install"
        export QUERY_CMD=$PACMAN_QUERY_CMD
        # Update
        sudo pacman -Syy --noconfirm
        sudo pacman -Syu --noconfirm
        # Check that yay is installed, otherwise install
        sudo pacman -S --needed base-devel
        if ! hash yay 2> /dev/null; then
            YAY_DIR=~/apps/yay-bin
            if ! [[ -d $YAY_DIR ]]; then
                git clone https://aur.archlinux.org/yay-bin.git $YAY_DIR
            fi
            (cd $YAY_DIR && makepkg -si)
            yay --gendb
            yay --devel --save
        fi
        ;;
    *)
        echo "unsupported linux $LINUX"
        exit 1
esac

if [[ $PRIVATE -eq 1 ]]; then
    folder_install passman
    folder_install keyringman
    exit
fi

$INSTALL_FUNC git
$INSTALL_FUNC xclip
$INSTALL_FUNC xsel
$INSTALL_FUNC curl
$INSTALL_FUNC wget
$INSTALL_FUNC openssh

############
# terminal #
############
folder_install pip
$INSTALL_FUNC nodejs npm
pip3 install manven
folder_install neovim
$INSTALL_FUNC fzf
$INSTALL_FUNC vifm
$INSTALL_FUNC ueberzug
$INSTALL_FUNC usbguard
$INSTALL_FUNC ctags
$INSTALL_FUNC tmux
$INSTALL_FUNC exa
$INSTALL_FUNC fd
folder_install rust
folder_install go
folder_install myrepos
#folder_install lf
$INSTALL_FUNC bat
$INSTALL_FUNC kitty
$INSTALL_FUNC vdirsyncer
$INSTALL_FUNC khal
$INSTALL_FUNC khard
$INSTALL_FUNC rofimoji
$INSTALL_FUNC noto-fonts-emoji
# folder_install todoist
folder_install broot
folder_install ripgrep
folder_install clipmenu
folder_install kmonad
copy_all_bin
case $LINUX in
    $UBUNTU)
        ;;
    $ARCH)
        yay_install pmount
        yay_install tealdeer
        yay_install zoxide
        folder_install bluetooth
        $INSTALL_FUNC xbindkeys
        $INSTALL_FUNC playerctl # Check for ubuntu
        # Enable user linger
        sudo loginctl enable-linger $USER
        setup_systemd_units # TODO check for ubuntu if needed for pcloud
        ;;
esac

if [[ $TERMINAL -eq 1 ]]; then
    echo "Only installing terminal related things"
else
    echo "Continue with full installation"
    $INSTALL_FUNC feh
    $INSTALL_FUNC dmenu
    $INSTALL_FUNC keepassxc
    folder_install herbstluftwm
    folder_install background
    $INSTALL_FUNC onboard
    case $LINUX in
        $UBUNTU)
            # folder_install no-title-bar
            # folder_install gnome-tweak-tool
            ;;
        $ARCH)
            yay_install timeshift
            yay_install gscreenshot
            $INSTALL_FUNC zathura-pdf-mupdf
            # To set default app
            $INSTALL_FUNC perl-file-mimeinfo
            $INSTALL_FUNC nemo
            # Tablet mode and screen rotation
            yay_install iio-sensor-proxy
            yay_install screenrotator-git
            yay_install detect-tablet-mode-git
            ;;
    esac
    folder_install qutebrowser
    folder_install latex
    folder_install swap_hibernate
    folder_install pcloud
fi

folder_install touchpad
folder_install ssh
folder_install zsh
