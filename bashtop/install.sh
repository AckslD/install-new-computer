[[ -d ~/apps/bashtop ]] && exit
git clone https://github.com/aristocratos/bashtop.git ~/apps/bashtop
ln -s ~/apps/bashtop/bashtop ~/.local/bin
