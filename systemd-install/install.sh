# Enable user linger
loginctl enable-linger $USER

# Copy service and timers
SYSTEMD_USER_DIR=~/.config/systemd/user
mkdir -p $SYSTEMD_USER_DIR
find . -type f \( -name "*.service" -o -name "*.timer" \) -exec cp {} $SYSTEMD_USER_DIR \;

# Enable the timers
find . -type f -name "*.timer" -exec systemctl enable --user {} \;

# Copy desktop files
APPLICATIONS_DIR=~/.local/share/applications
find . -type f -name "*.desktop" -exec cp {} $APPLICATIONS_DIR \;
