source install_functions.sh

# Install zsh
$INSTALL_FUNC zsh
sudo chsh -s $(which zsh)

# Install oh-my-zsh
if ! [[ -d ~/.config/oh-my-zsh ]]; then
    mkdir -p ~/.config
    export ZSH="$HOME/.config/oh-my-zsh"
    sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    # extra plugins
    omz_plugin zsh-users zsh-syntax-highlighting
    omz_plugin zsh-users zsh-autosuggestions
    omz_plugin zsh-users zsh-history-substring-search
    omz_plugin jeffreytse zsh-vi-mode
    omz_plugin djui alias-tips
fi

function omz_plugin() {
    git clone "https://github.com/$1/$2.git ~/.config/oh-my-zsh/custom/plugins/$2"
}
