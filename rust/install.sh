which cargo 2> /dev/null && exit
source install_functions.sh

# Install rust
case $LINUX in
    $UBUNTU)
        curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs > rustup.sh
        if [[ $QUIET -eq 1 ]]; then
            sh rustup.sh -y
        else
            sh rustup.sh
        fi
        rm rustup.sh
        ;;
    $ARCH)
        $INSTALL_FUNC rustup
        rustup install stable
        rustup default stable
        ;;
esac
