SHELL := /bin/bash

quick: bin systemd

terminal:
	bash install.sh -t -q

all:
	bash install.sh -q

terminal-force:
	bash install.sh -t -f

all-force:
	bash install.sh -f

bin:
	source ./install_functions.sh && copy_all_bin

systemd:
	source ./install_functions.sh && setup_systemd_units

private:
	bash install.sh -p
