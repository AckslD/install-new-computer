which qutebrowser 2> /dev/null && exit
source install_functions.sh

$INSTALL_FUNC qutebrowser
# For pdf viewing in browser
$INSTALL_FUNC pdfjs

if [[ $QUIET -eq 1 ]]; then
    yn=y
else
    read -p "Do you want to install language pack for spelling? (y/n)" yn
fi

if [[ "$yn" == "y" ]]; then
    QUTEBROWSER_DIR=~/Dev/qutebrowser
    if ! [[ -d $QUTEBROWSER_DIR ]]; then
        git clone https://github.com/qutebrowser/qutebrowser.git $QUTEBROWSER_DIR
    fi
    VENV_DIR=$QUTEBROWSER_DIR/.venv
    pip3 install virtualenv
    python3 $QUTEBROWSER_DIR/scripts/mkvenv.py --venv-dir $VENV_DIR --virtualenv
    source $VENV_DIR/bin/activate
    python3 $QUTEBROWSER_DIR/scripts/dictcli.py install en-GB
    deactivate
fi

xdg-settings set default-web-browser qutebrowser.desktop
