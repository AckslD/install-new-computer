which clipmenu 2> /dev/null && exit
source install_functions.sh
case $LINUX in
    $UBUNTU)
        echo "TODO not setting up clipmenu since on ubuntu"
        ;;
    $ARCH)
        # Install normally
        $INSTALL_FUNC clipmenu

        # Update binaries
        git clone https://github.com/cdown/clipmenu.git ~/Dev/clipmenu
        for file in clipdel clipctl clipfsck clipmenu clipmenud; do
            sudo ln -sf ~/Dev/clipmenu/$file /usr/bin
        done

        # Update service
        DROPINDIR=~/.config/systemd/user/clipmenud.service.d
        echo "creating folder $DROPINDIR"
        mkdir -p $DROPINDIR
        DROPINFILE=$DROPINDIR/override.conf
        echo "creating file $DROPINFILE"
        echo "[Service]" > $DROPINFILE
        echo "Environment=CM_LAUNCHER=rofi CM_IGNORE_WINDOW=KeePass CM_SELECTIONS=clipboard" >> $DROPINFILE
        echo "PassEnvironment=CM_LAUNCHER CM_IGNORE_WINDOW CM_SELECTIONS" >> $DROPINFILE
        ;;
esac
