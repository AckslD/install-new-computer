which herbstluftwm 2> /dev/null && exit
source install_functions.sh

if [[ "$LINUX" == "$ARCH" ]]; then
    $INSTALL_FUNC xorg
    $INSTALL_FUNC xorg-xinit
    $INSTALL_FUNC alsa-utils
    $INSTALL_FUNC pulseaudio
    $INSTALL_FUNC pulseaudio-alsa
    sudo groupmems -g audio -a $USER
fi
$INSTALL_FUNC herbstluftwm
$INSTALL_FUNC rofi
$INSTALL_FUNC i3lock
$INSTALL_FUNC dunst
yay_install batsignal

# TODO Haven't checked for ubuntu
$INSTALL_FUNC ttf-font-awesome

# Install Siji fonts
if [[ -d ~/apps/fonts/siji ]]; then
    echo "Fonts siji already installed"
else
    mkdir -p ~/apps/fonts
    git clone https://github.com/stark/siji ~/apps/fonts/siji
    cd ~/apps/fonts/siji
    ./install.sh
    cd
fi

# Install lemonbar and conky
case $LINUX in
    $UBUNTU)
        $INSTALL_FUNC conky-all
        $INSTALL_FUNC lemonbar
        ;;
    $ARCH)
        $INSTALL_FUNC conky
        yay_install lemonbar-xft-git
        $INSTALL_FUNC xorg-fonts-misc
        ;;
esac

if [[ -d ~/Dev/barpyrus ]]; then
    echo "barpyrus already installed"
else
    git clone -b develop https://github.com/AckslD/barpyrus.git ~/Dev/barpyrus
    rm -f ~/.config/herbstluftwm/panel.sh
    mkdir -p ~/.config/herbstluftwm
    ln -s ~/Dev/barpyrus/barpyrus.py ~/.config/herbstluftwm/panel.sh
fi
