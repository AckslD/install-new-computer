source install_functions.sh

# $INSTALL_FUNC no-title-bar
if ! [[ -d ~/apps/no-title-bar ]]; then
    git clone https://github.com/franglais125/no-title-bar.git ~/apps
    make -C ~/apps/no-title-bar install
    gnome-shell-extension-tool -e no-title-bar@franglais125.gmail.com
fi
