which mr 2> /dev/null && exit
source install_functions.sh

# Install myrepos (mr)
case $LINUX in
    $UBUNTU)
        $INSTALL_FUNC myrepos
        ;;
    $ARCH)
        yay_install myrepos
        ;;
esac
