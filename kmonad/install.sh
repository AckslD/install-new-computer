source install_functions.sh

yay_install kmonad-bin

sudo groupadd uinput
sudo usermod -aG uinput $USER
sudo usermod -aG input $USER
sudo cp kmonad/99-uinput.rules /lib/udev/rules.d/
sudo modprobe uinput
