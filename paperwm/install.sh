# Install PaperWM
mkdir -p ~/apps
if ! [[ -d ~/apps/PaperWM ]]; then
    git clone https://github.com/paperwm/PaperWM.git ~/apps
    cd PaperWM
    bash ~/apps/PaperWM/install.sh
    gsettings set org.gnome.shell disable-user-extensions false
fi
