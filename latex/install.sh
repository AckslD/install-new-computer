source install_functions.sh
if [[ $QUIET -eq 1 ]]; then
    yn="n"
else
    read -p "Do you want to install latex (texlive) and additional packages (large)..(y/n)" yn
fi
if [ "$yn" == "y" ]; then
    case $LINUX in
        $UBUNTU)
            $INSTALL_FUNC texlive-latex-base texlive-publishers texlive-science texlive-fonts-extra texlive-pstricks
            ;;
        $ARCH)
            $INSTALL_FUNC texlive-most
            yay_install tllocalmgr-git
            ;;
    esac
fi
