which broot 2> /dev/null && exit
source install_functions.sh
case $LINUX in
    $UBUNTU)
        cargo_install broot
        ;;
    $ARCH)
        $INSTALL_FUNC broot
        ;;
esac
