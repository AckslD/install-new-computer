# Swap and hibernate
if [[ $QUIET -eq 1 ]]; then
    yn="n"
else
    read -p "Do you want information about setting up a swapfile and hibernation here now? (y/n)" yn
fi

if [ "$yn" == "y" ]; then
    echo "If needed, first disable secure boot."
    echo ""
    echo "Check size of swap:"
    echo "    free"
    echo "if less than RAM create a larger."
    echo "Create a swap file if it does not exist:"
    echo "    sudo swapoff -a"
    echo "    sudo fallocate -l 8GB /swapfile"
    echo "    sudo chmod 600 /swapfile"
    echo "    sudo mkswap /swapfile"
    echo "    sudo swapon /swapfile"
    echo "    echo '/swapfile swap swap defaults 0 0' | sudo tee -a /etc/fstab<Paste>"
    echo ""
    echo "Verify swap file partition:"
    echo "    sudo findmnt -no SOURCE,UUID -T /swapfile"
    echo ""
    echo "Configure uswsup (install if needed):"
    echo "    sudo dpkg-reconfigure -pmedium uswsusp"
    echo ""
    echo "Note offset of swapfile:"
    echo "    sudo swap-offset /swapfile"
    echo ""
    echo "Configure grub, write to /etc/default/grub the following:"
    echo "    GRUB_CMDLINE_LINUX_DEFAULT="resume=UUID=20562a02-cfa6-42e0-bb9f-5e936ea763d0 resume_offset=34818 quiet splash""
    echo ""
    echo "Update grub:"
    echo "    sudo update-grub"
    echo ""
    echo "Create the following /etc/initramfs-tools/conf.d/resume:"
    echo "    RESUME=UUID=20562a02-cfa6-42e0-bb9e-5e936ea763d0 resume_offset=34816"
    echo "    # Resume from /swapfile"
    echo ""
    echo "Update initramfs:"
    echo "    sudo update-initramfs -u -k all"
fi
