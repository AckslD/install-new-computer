# Install new computer
To install things for terminal run:
```bash
make
```
to also install tiling manager etc
```bash
make all
```
Alternatively do `make terminal-force`/`make all-force` to avoid quering for input.

## Arch
To install arch, create an iso by running `arch/prep-iso.sh` which creates a new iso with an install script.
From the booted usb, run `setup.sh`.

## Distributions
Tested for:
* Ubuntu 19.04
* Ubuntu 19.10
* Ubuntu 20.04
* EndeavourOS
* Arch
