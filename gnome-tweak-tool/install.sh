source install_functions.sh
# For making caps lock esc on ubuntu
$INSTALL_FUNC gnome-tweak-tool
echo "You can now open gnome-tweaks and set caps-lock to esc"
echo "Additionally you can set the following key-bindings:"
echo "    Launch web browser:             Ctrl+Alt+I"
echo "    Move to workspace above:        Super+K"
echo "    Move to workspace below:        Super+J"
echo "    Move window one workspace down: Shift+Super+J"
echo "    Move window one workspace up:   Shift+Super+K"
echo "    Lock screen:                    Ctrl+Alt+L"
echo "    Hide window:                    Ctrl+Alt+H"
echo "    Maximize window:                Shift+Super+L"
echo "    Restore window:                 Shift+Super+H"
echo "    View split on left:             Super+H"
echo "    View split on right:            Super+L"
echo "    vim -> xclip (custom):          Ctrl+Space"
echo "        (with command: gnome-terminal -e 'fish -c vimxclip')"
read -n 1 -p "Press enter when finished"
