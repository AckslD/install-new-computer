#Install lf
if [[ -e ~/Dev/golib/bin/lf ]]; then
    echo "lf already installed"
    exit
fi

export GOPATH=~/Dev/golib
go get -u github.com/gokcehan/lf
