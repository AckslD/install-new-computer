source install_functions.sh

# Install pip
case $LINUX in
    $UBUNTU)
        $INSTALL_FUNC python3-pip
        ;;
    $ARCH)
        $INSTALL_FUNC python-pip
        ;;
esac
