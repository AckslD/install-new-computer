set -e

# Create custom local repository for timeshift
# UNCOMMENT to include timeshift
# if ! [[ -d "timeshift" ]]; then
#     # TODO needs to fix relative paths to .pkg.tar.zst
#     echo "Clone timeshift"
#     git clone https://aur.archlinux.org/timeshift.git
#     echo "Make timeshift package"
#     (cd timeshift && makepkg -sr)
#     echo "Add timeshift to repo"
#     repo-add timeshift.db.tar.gz timeshift/timeshift-*.pkg.tar.zst
# fi

# Clear current
rm -r archlive

# Use releng profile
cp -r /usr/share/archiso/configs/releng archlive

# Add package config
# UNCOMMENT to include timeshift
# cp pacman.conf archlive
# cp packages.x86_64 archlive

# Add install scripts
cp setup.sh archlive/airootfs
cp root-setup.sh archlive/airootfs

# Create iso
WORK_DIR=/tmp/archiso-tmp/releng
mkdir -p $WORK_DIR
sudo mkarchiso -v -w $WORK_DIR archlive

# Remove working directory
sudo rm -rf $WORK_DIR

# Change owner
sudo chown "$USER" out/*
