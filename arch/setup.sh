set -e

function notify_file() {
    echo "Updated $1 to be (q to quit):"
    cat $1 | less
    read -n 1 -p "Press enter to continue, ctrl-d to cancel..."
}

echo "Setting up arch linux, abort with ctrl-c at any point to make changes and re-run again"

# Boot mode
[[ -d /sys/firmware/efi/efivars ]] || read -p "Boot mode not EFI, continue? (y/n)" yn
[[ "$yn" == "n" ]] && echo "Aborting!" && exit

# Internet
read -p "Connect to internet? (y/n)" yn
if [[ "$yn" == "y" ]]; then
    iwctl device list
    read -p "Enter device name from above (default wlan0): " NET_DEVICE
    [[ -z $NET_DEVICE ]] && NET_DEVICE="wlan0"
    iwctl station $NET_DEVICE scan
    iwctl station $NET_DEVICE get-networks
    read -p "Enter network name from above: " NET_NAME
    iwctl station $NET_DEVICE connect "$NET_NAME"
    read -n 1 -p "Checking internet connection (enter to start, ctrl-c to cancel)"
    ping archlinux.org
fi

# System clock
read -p "Set system clock? (y/n)" yn
if [[ "$yn" == "y" ]]; then
    timedatectl set-ntp true
    read -p "Enter timezone (default Europe/Amsterdam): " TIMEZONE
    [[ -z $TIMEZONE ]] && TIMEZONE=Europe/Amsterdam
    timedatectl set-timezone $TIMEZONE
    timedatectl status
    read -n 1 -p "Check time status above (enter to continue)"
fi


# Partitions
fdisk -l
read -p "Input path to root-partition (default /dev/sda2): " ROOT_PART
[[ -z $ROOT_PART ]] && ROOT_PART="/dev/sda2"
read -p "Format root partition $ROOT_PART? (y/n)" yn
if [[ "$yn" == "y" ]]; then
    read -p "Confirm root parition $ROOT_PART, it will be erased (y/n)" yn
    if ! [[ "$yn" == "y" ]]; then
        echo "Aborting!"
        exit
    fi
    read -p "Overwrite existing data (takes a long time)? (y/n)" yn
    if [[ "$yn" == "y" ]]; then
        cryptsetup open --type plain -d /dev/urandom $ROOT_PART to_be_wiped
        dd if=/dev/zero of=/dev/mapper/to_be_wiped status=progress
        cryptsetup close to_be_wiped
    fi
    read -p "Encrypt root partition? (y/n)" yn
    if [[ "$yn" == "y" ]]; then
        CRYPT_ROOT="cryptroot"
        CRYPT_DEVICE="/dev/mapper/$CRYPT_ROOT"
        echo "Encrypting partition $ROOT_PART to $CRYPT_DEVICE"
        cryptsetup -y -v luksFormat $ROOT_PART
        cryptsetup open $ROOT_PART $CRYPT_ROOT
        mkfs.ext4 $CRYPT_DEVICE
        echo "Mounting encrypted root $CRYPT_DEVICE to /mnt"
        mount $CRYPT_DEVICE /mnt
    else
        mkfs.ext4 $ROOT_PART
        echo "Mounting $ROOT_PART to /mnt"
        mount $ROOT_PART /mnt
    fi
else
    if [[ -z "$(mount | grep '/mnt ')" ]]; then
        read -p "Is the root partition formatted? (y/n)" yn
        if [[ "$yn" == "y" ]]; then
            CRYPT_ROOT="cryptroot"
            CRYPT_DEVICE="/dev/mapper/$CRYPT_ROOT"
            if ! [[ -e $CRYPT_DEVICE ]]; then
                cryptsetup open $ROOT_PART $CRYPT_ROOT
            fi
            mount $CRYPT_DEVICE /mnt
        else
            echo "Mounting $ROOT_PART to /mnt"
            mount $ROOT_PART /mnt
        fi
    fi
fi

read -p "Input path to efi-partition (default /dev/sda1): " EFI_PART
[[ -z $EFI_PART ]] && EFI_PART="/dev/sda1"
read -p "Do you also want to format the EFI partition $EFI_PART? (y/n) " yn
if [[ "$yn" == "y" ]]; then
    mkfs.fat -F32 $EFI_PART
fi
if [[ -z "$(mount | grep '/mnt/boot ')" ]]; then
    echo "Mounting $EFI_PART to /mnt/boot"
    EFI_DIR="/mnt/boot"
    if ! [[ -d $EFI_DIR ]]; then
        mkdir $EFI_DIR
    fi
    mount $EFI_PART $EFI_DIR
fi

# Packages
echo "Selecting fastest mirrors"
reflector
read -p "Install packages (recommended)? (y/n)" yn
if [[ "$yn" == "y" ]]; then
    echo "Installing essential packages"
    pacstrap /mnt base linux linux-firmware iwd vim man-db man-pages texinfo sudo
fi

# fstab
read -p "Generate fstab? (y/n)" yn
if [[ "$yn" == "y" ]]; then
    echo "Generating fstab"
    genfstab -U /mnt >> /mnt/etc/fstab
    notify_file /mnt/etc/fstab
fi

# Chroot
read -p "Change root and continue setup (recommended)? (y/n)" yn
if [[ "$yn" == "y" ]]; then
    cp /root-setup.sh /mnt
    chmod +x /mnt/root-setup.sh
    arch-chroot /mnt ./root-setup.sh
    rm /mnt/root-setup.sh
fi

# Unmount
read -p "Confirm unmount /mnt? (y/n)" yn
if [[ "$yn" == "y" ]]; then
    umount -R /mnt
fi
read -p "Reboot now? (y/n)" yn
if [[ "$yn" == "y" ]]; then
    reboot
fi
