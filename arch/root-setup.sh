set -e

function notify_file() {
    echo "Updated $1 to be:"
    cat $1 | less
    read -n 1 -p "Press enter to continue, ctrl-d to cancel..."
}

# Timezone
read -p "Enter timezone (default Europe/Amsterdam): " TIMEZONE
[[ -z $TIMEZONE ]] && TIMEZONE=Europe/Amsterdam
ln -sf /usr/share/zoneinfo/$TIMEZONE /etc/localtime
hwclock --systohc

# Localization
read -p "Confirm setting up localization to en_US en_GB sv_SE? (y/n)" yn
if [[ "$yn" == "y" ]]; then
    for locale in en_US en_GB sv_SE; do
        sed -i "s/^#\($locale.UTF-8 UTF-8\)/\1/g" /etc/locale.gen
    done
    notify_file /etc/locale.gen
fi
locale-gen
read -p "Confirm setting LANG to en_US? (y/n)" yn
if [[ "$yn" == "y" ]]; then
    echo "LANG=en_US.UTF-8" >> /etc/locale.conf
    notify_file /etc/locale.conf
fi

# Network configuration
read -p "Enter host name (empty cancels)" HOSTNAME
if [[ -n $HOSTNAME ]]; then
    echo $HOSTNAME > /etc/hostname
    notify_file /etc/hostname

    HOSTS="127.0.0.1\tlocalhost\n::1\t\tlocalhost\n127.0.1.1\t$HOSTNAME.localdomain $HOSTNAME"
    echo "Will configure the following hosts:\n$HOSTS"
    read -p "Confirm? (y/n): " yn
    if [[ "$yn" == "y" ]]; then
        echo -e $HOSTS > /etc/hosts
        notify_file /etc/hosts
    fi
fi

# Microcode
read -p "Install microcode (recommended)? (y/n)" yn
if [[ "$yn" == "y" ]]; then
    AMD="amd"
    INTEL="intel"
    cat /proc/cpuinfo
    while [[ -z $CPU_TP ]]; do
        read -p "Is cpu amd or intel? " CPU_ANS
        case $CPU_ANS in
            $AMD|$INTEL)
                CPU_TP=$CPU_ANS
                ;;
            "")
                echo "Neither amd or intel? Will not add microcode but you should check this!"
                break
                ;;
            *)
                echo "Unknown chip type, try again or enter nothing to ignore adding microcode"
                ;;
        esac
        case $CPU_TP in
            $AMD)
                pacman -S --needed amd-ucode
                ;;
            $INTEL)
                pacman -S --needed intel-ucode
                ;;
        esac
    done
fi

# Boot loader
pacman -S --needed grub
read -p "Install grub? (y/n)" yn
if [[ "$yn" == "y" ]]; then
    read -p "Provide path to EFI directory (default /boot): " EFI_DIR
    if [[ -z $EFI_DIR ]]; then
        EFI_DIR="/boot"
    fi
    read -p "Confirm install grub using EFI dir $EFI_DIR? (y/n)" yn
    if [[ "$yn" == "y" ]]; then
        pacman -S --needed efibootmgr
        grub-install --target=x86_64-efi --efi-directory=$EFI_DIR --bootloader-id=GRUB
    else
        echo "Aborting!"
        exit
    fi
else
    echo "Not installing grub on EFI partition, assuming it is installed..."
fi

# Encrypt configs
read -p "Did you encrypt the root partition and want to update mkinitcpio and grub? (y/n)" yn
if [[ "$yn" == "y" ]]; then
    fdisk -l
    read -p "Provide path to root partition (default /dev/sda2): " ROOT_PART
    [[ -z $ROOT_PART ]] && ROOT_PART="/dev/sda2"
    echo "Update mkinitcpio"
    sed -i "s/^HOOKS=(\(.*\))/HOOKS=(\1 encrypt)/" /etc/mkinitcpio.conf
    notify_file /etc/mkinitcpio.conf
    echo "Configure GRUB kernel parameters"
    lsblk -f
    ROOT_UUID=$(lsblk -f | grep $(basename $ROOT_PART) | awk -F ' ' '{print $4}')
    read -p "Enter UUID of root partition (default $ROOT_UUID): " UUID_ANS
    if [[ -n $UUID_ANS ]]; then
        ROOT_UUID=$UUID_ANS
    fi
    if [[ -z $ROOT_UUID ]]; then
        echo "No UUID given"
        echo "Aborting!"
        exit
    fi
    KERNEL_PARAMS="cryptdevice=UUID=$ROOT_UUID:cryptroot root=/dev/mapper/cryptroot"
    GRUB_PARAMS="GRUB_CMDLINE_LINUX_DEFAULT=\"$KERNEL_PARAMS\""
    # TODO ENABLE_CRYPT?
    echo $GRUB_PARAMS > /etc/default/grub
    notify_file /etc/default/grub
fi

echo "Make grub config"
grub-mkconfig -o /boot/grub/grub.cfg
echo "Create initial ram disk env"
mkinitcpio -P

# Network services
read -p "Do you want to activate network services (recommended)? (y/n) " yn
if [[ "$yn" == "y" ]]; then
    systemctl enable systemd-networkd
    systemctl enable systemd-resolved
    systemctl enable iwd
    read -p "Do you want to make us of iwd's DHCP (recommended)? (y/n) " yn
    if [[ "$yn" == "y" ]]; then
        mkdir /etc/iwd
        echo -e "[General]\nEnableNetworkConfiguration=true\n\n" > /etc/iwd/main.conf
        echo -e "[Network]\nNameResolvingService=systemd" >> /etc/iwd/main.conf
        notify_file /etc/iwd/main.conf
    fi
fi

# Root password
echo "Enter password for root"
passwd

# Create a user
read -p "Input username: " USERNAME
useradd -m $USERNAME
echo "Enter password for user $USERNAME"
passwd $USERNAME
read -p "Add $USERNAME to sudoers? (y/n)" yn
if [[ "$yn" == "y" ]]; then
    read -n 1 -p "Enter '$USERNAME ALL=(ALL) ALL' into the file that will open (enter to continue)..."
    export EDITOR=/usr/bin/vim
    visudo
fi

# Add to netdev for iwctl
groupadd netdev
usermod -aG netdev "$USERNAME"

# Additional setup
read -p "Download personal setup scripts? (y/n)" yn
if [[ "$yn" == "y" ]]; then
    pacman -S --needed git make base-devel lsb-release
    DEV_DIR=/home/$USERNAME/Dev
    mkdir $DEV_DIR
    GIR_URL=https://gitlab.com/AckslD
    for repo in install-new-computer config; do
        git clone $GIT_URL/$repo.git $DEV_DIR/$repo
    done
    echo "After restarting, and login into $USERNAME, do:"
    echo "cd $DEV_DIR/install-new-computer"
    echo "make"
    echo "cd $DEV_DIR/config"
    echo "make"
fi

read -n 1 -p "Will now exit chroot (ctrl-d to cancel)"
exit
