which rg 2> /dev/null && exit
source install_functions.sh
case $LINUX in
    $UBUNTU)
        cargo_install ripgrep
        ;;
    $ARCH)
        $INSTALL_FUNC ripgrep
        ;;
esac
